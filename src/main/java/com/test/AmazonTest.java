package com.test;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AmazonTest extends DriverFactory{
	

	
	@Test(priority=0)
	public void Search()throws InterruptedException {
		
	WebElement searchBox=driver.findElement(By.id("twotabsearchtextbox"));
	searchBox.sendKeys("Iphone x");
	driver.findElement(By.id("nav-search-submit-button")).click();
	Thread.sleep(2000);
	driver.findElement(By.linkText("Apple iPhone XR (128GB) - Coral")).click();
	
	String parentWindow = driver.getWindowHandle();
	Set<String> handles =  driver.getWindowHandles();
	   for(String windowHandle  : handles)
	       {
	       if(!windowHandle.equals(parentWindow))
	          {
	          driver.switchTo().window(windowHandle);
	          }
	       }
	
	}
	@Test(priority=2)
 public void changeQuantity()throws NoSuchElementException{
	 
	 driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	 Select quantity=new Select(driver.findElement(By.xpath("//select[@id='quantity']")));
	 quantity.selectByValue("2");
	
 }
	@Test(priority=1)
	public void addToCart()throws InterruptedException{
		
		
		driver.findElement(By.xpath("//input[@name='submit.add-to-cart']")).click();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		String itemAdded=driver.findElement(By.xpath("//span[@class='a-button a-button-base attach-button-large attach-cart-button']")).getText();
		driver.findElement(By.xpath("//span[@class='a-button a-button-base attach-button-large attach-cart-button']")).click();
		Assert.assertTrue(true,itemAdded);
		
	}
	@Test(priority=3)
	public void removeProduct()throws InterruptedException{
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[@class='a-size-small sc-action-delete']/span")).click();
		String productRemoved=driver.findElement(By.xpath("//*[contains(text(), 'was removed from Shopping Cart.')]")).getText();
		
        Assert.assertTrue(true, productRemoved);
		
	}
	}


